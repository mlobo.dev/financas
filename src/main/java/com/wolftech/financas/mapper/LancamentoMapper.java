package com.wolftech.financas.mapper;


import com.wolftech.financas.dto.LancamentoDTO;
import com.wolftech.financas.dto.UsuarioDTO;
import com.wolftech.financas.model.Lancamento;
import com.wolftech.financas.model.Usuario;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface LancamentoMapper extends BaseMapper<Lancamento, LancamentoDTO> {

}
