package com.wolftech.financas.mapper;


import com.wolftech.financas.dto.LancamentoCadastroDTO;
import com.wolftech.financas.dto.LancamentoDTO;
import com.wolftech.financas.model.Lancamento;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface LancamentoCadastroMapper extends BaseMapper<Lancamento, LancamentoCadastroDTO> {

}
